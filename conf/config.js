/**
 * Example configurations for development and for production.
 * This config file will be changed while deploying, so use it
 * just like an example or doc.
 */
module.exports = {
    dev: {
        port: 4003,
        host: "localhost",
        // redis config
        redis_port: '6379',
        redis_host: 'localhost',
        queue_prefix: 'analytics:',
        queue_names: {
            users: 'users',
            traps: {
                sessions: 'traps:sessions',
                purchase: 'traps:purchase'
            }
        }
    },
    prod: {

    },
    test: {
        port: 5004,
        host: "localhost",
        // redis config
        redis_port: '6379',
        redis_host: 'localhost',
        queue_prefix: 'test:analytics:',
        queue_names: {
            users: 'users',
            traps: {
                sessions: 'traps:sessions',
                purchase: 'traps:purchase'
            }
        }
    },
    unittest: {
        port: 5003,
        host: "localhost",
        // redis config
        redis_port: '6379',
        redis_host: 'localhost',
        queue_prefix: 'test:analytics:',
        queue_names: {
            users: 'users',
            traps: {
                sessions: 'traps:sessions',
                purchase: 'traps:purchase'
            }
        }
    }
};