**Please see main repository for details:** https://bitbucket.org/combocats/hive-deploy

# Hive-analytics-collector

Provides API for collecting analytics. Requires user's authentication for all the requests.

### Tests

To run tests use commands:

 + `make all` - run all unit and e2e tests
 + `make unit` - run all unit tests
 + `make e2e` - run all e2e tests