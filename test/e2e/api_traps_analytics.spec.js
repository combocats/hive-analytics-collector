var hooks   = require('../e2e_hooks');
require('should');
var Modules = require('../modules');
var errors  = require(Modules.errors);

var request     = require('request');
var testUsers   = require('../test_users');
var activeUser  = testUsers[Object.keys(testUsers)[0]];

var request = require('request').defaults({
    headers: {
        'Content-Type': 'application/json'
    }
});

var config = require('hive-common').settings.config;

function url(url) {
    return "http://localhost:" + config.port + "/api/analytics/traps" + url + "?accessToken=" + activeUser._id;
}

describe('lib.api_traps_analytics', function() {

    before(hooks.before);
    after(hooks.after);

    describe('/session', function() {

        it('should push data to traps session queue', function(done) {
            var data = {
                start: new Date('Wed Apr 16 2014 11:01:14'),
                end: new Date('Wed Apr 16 2014 11:31:14')
            };
            request({
                url: url("/session"),
                method: "POST",
                json: data
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                done();
            });
        });

    });

    describe('/purchase', function() {

        it('should push data to traps purchase queue', function(done) {
            var data = {
                timestamp: new Date('Wed Apr 16 2014 11:01:14'),
                amount: 4,
                purchase_id: '1234567890',
                type: '12'
            };
            request({
                url: url("/purchase"),
                method: "POST",
                json: data
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                done();
            });
        });

    });

});