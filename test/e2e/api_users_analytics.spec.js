var hooks   = require('../e2e_hooks');
require('should');
var Modules = require('../modules');
var errors  = require(Modules.errors);

var request     = require('request');
var testUsers   = require('../test_users');
var activeUser  = testUsers[Object.keys(testUsers)[0]];

var request = require('request').defaults({
    headers: {
        'Content-Type': 'application/json'
    }
});

var config = require('hive-common').settings.config;

function url(url) {
    return "http://localhost:" + config.port + "/api/analytics" + url + "?accessToken=" + activeUser._id;
}

describe('lib.api_users_analytics', function() {

    before(hooks.before);
    after(hooks.after);

    describe('/users', function() {

        it('should push data to users queue', function(done) {
            var data = {
                id: '529c19753255720000000001',
                created: new Date(),
                country: 'RU',
                channel: 'fb'
            };
            request({
                url: url("/users"),
                method: "POST",
                json: data
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                done();
            });
        });

    });

});