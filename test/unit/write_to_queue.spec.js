process.env.NODE_ENV = 'unittest';
var should  = require('should');
var _       = require('lodash');

var Modules = require('../modules');

describe('lib.write_to_queue', function() {

    describe('constructor', function() {

        var QueueWriter = require(Modules['write_to_queue']);
        it('should properly create QueueWriter instance', function() {
            var writer = new QueueWriter({
                key: 'test',
                client: {}
            });
            writer.should.be.instanceOf(QueueWriter);
            writer.push.should.be.type('function');
        });

        it('should throw error on attempt to create instance without key option', function() {
            (function() {
                new QueueWriter({
                    client: {}
                });
            }).should.throw();
        });

        it('should throw error on attempt to create instance without client option', function() {
            (function() {
                new QueueWriter({
                    key: 'test'
                });
            }).should.throw();
        });

    });

    describe('push', function() {

        var QueueWriter = require(Modules['write_to_queue']);
        it('should properly starts reading queue', function(done) {
            var data = { ok: true };
            var client = {
                RPUSH: function(key, strData) {
                    strData.should.be.equal(JSON.stringify(data));
                    _.last(arguments)(null, 1);
                    done();
                }
            };
            var writer = new QueueWriter({
                key: 'test',
                client: client
            });
            writer.push(data);
        });

    });

});