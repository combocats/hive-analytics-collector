var path = require('path');
var _    = require('lodash');

var Modules = {
    'errors'              : './lib/errors',
    'write_to_queue'      : './lib/write_to_queue'
};

var absolutPaths = {};
_.forEach(Modules, function(value, key) {
    absolutPaths[key] = path.resolve(value);
});

module.exports = absolutPaths;