var errors = require('hive-common').errors;

var AnalyticsErrors = {
    COULD_NOT_PUSH_TO_QUEUE: {
        status: 500,
        code: 400000,
        message: 'Could not push data to redis queue'
    },
    END_SHOULD_BE_ABOVE_START: {
        status: 400,
        code: 400001,
        message: 'Session end date should be above start date'
    }
};

module.exports = errors(
    AnalyticsErrors
);