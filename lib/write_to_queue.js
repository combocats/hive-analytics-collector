var Q        = require('q');
var log      = require('hive-common').logger('write_to_queue');
var config   = require('hive-common').settings.config;
var _        = require('lodash');
var should   = require('should');

var errors   = require('./errors');

var QueueWriter = function(options) {
    options.should.be.ok;
    options.should.have.properties(['key', 'client']);
    _.extend(this, options);
};

/**
 * Push analytic data into redis queue.
 * @returns {promise}
 */
QueueWriter.prototype.push = function(data) {
    var self = this;
    var strData = JSON.stringify(data);
    return Q.ninvoke(self.client, 'RPUSH', self.key, strData)
        .catch(function(error) {
            return Q.reject(errors.COULD_NOT_PUSH_TO_QUEUE({key: self.key, error: error}));
        });
};

module.exports = QueueWriter;