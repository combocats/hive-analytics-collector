var errors = require('./errors');
var log    = require('hive-common').logger('ensure_authenticated');
/**
 * Ensure authenticated middleware. Checks that req.session has `passport`
 * property and `passport.user` is defined. Otherwise returns error.
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
module.exports = function(req, res, next) {
    log.info("ensure authnticated called");
   if(req.session && req.session.passport && req.session.passport.user) {
       log.debug("user is authenticated");
       req.user = req.session.passport.user;
       return next();
   } else {
        log.warn("user is not authenticated", {cookie: req.cookies['connect.sid'], session: req.session});
        return next(errors.USER_NOT_AUTHENTICATED());
   }
};