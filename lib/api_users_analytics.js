var express = require('express');
var log     = require('hive-common').logger('api_users_analytics');
var config  = require('hive-common').settings.config;

var redis   = require('redis');
var redisClient  = redis.createClient(config.redis_port, config.redis_host);

var app = module.exports = express();

var ensureAuthenticated = require('./ensure_authenticated');
var QueueWriter = require('./write_to_queue');

var Queue = new QueueWriter({
    key: config.queue_prefix + config.queue_names.users,
    client: redisClient
});

app.use(ensureAuthenticated);

var usersAPIValidator = require('./validate_api_users');
app.post('/users', usersAPIValidator.check, function(req, res, next) {
    log.info("users analytics called: ", req.body);

    req.body.id = req.user;

    Queue.push(req.body)
        .then(function() {
            res.data = null;
            next();
        })
        .catch(next);
});