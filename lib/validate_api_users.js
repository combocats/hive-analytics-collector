var V   = require('hive-common').validate;

var Validator = {};

Validator.check = function(req, res, next) {
    V.chain([
        V.pickOnly(req, 'body', ['created', 'country', 'channel']),
        V.checkTypes(req.body, {country: String, channel: String}),
        V.mustHaveKeys(req.body, ['created', 'country', 'channel'])
    ], next);
};

module.exports = Validator;