var V   = require('hive-common').validate;
var Q   = require('q');
var moment = require('moment');
var errors = require('./errors');

var Validator = {};

var endAboveStart = function(start, end) {
    if(!start || !end) return Q(true);
    try {
        return moment(end).diff(start) > 0 ? Q(true) : Q.reject(errors.END_SHOULD_BE_ABOVE_START());
    } catch (err) {
        return Q.reject(errors.END_SHOULD_BE_ABOVE_START());
    }
};

Validator.session = function(req, res, next) {
    V.chain([
        V.pickOnly(req, 'body', ['start', 'end']),
        V.mustHaveKeys(req.body, ['start', 'end']),
        endAboveStart(req.body.start, req.body.end)
    ], next);
};

Validator.purchase = function(req, res, next) {
    V.chain([
        V.pickOnly(req, 'body', ['timestamp', 'amount', 'purchase_id', 'type']),
        V.mustHaveKeys(req.body, ['timestamp', 'amount', 'purchase_id', 'type'])
    ], next);
};

module.exports = Validator;