var express = require('express');
var log     = require('hive-common').logger('api_traps_analytics');
var config  = require('hive-common').settings.config;

var redis   = require('redis');
var redisClient  = redis.createClient(config.redis_port, config.redis_host);

var app = module.exports = express();

var ensureAuthenticated = require('./ensure_authenticated');
var QueueWriter = require('./write_to_queue');

var SessionQueue = new QueueWriter({
    key: config.queue_prefix + config.queue_names.traps.sessions,
    client: redisClient
});

var PurchaseQueue = new QueueWriter({
    key: config.queue_prefix + config.queue_names.traps.purchase,
    client: redisClient
});

app.use(ensureAuthenticated);

var trapsAPIValidator = require('./validate_api_traps');
app.post('/session', trapsAPIValidator.session, function(req, res, next) {
    log.info("traps session analytics called: ", req.body);

    req.body.id = req.user;

    SessionQueue.push(req.body)
        .then(function() {
            res.data = null;
            next();
        })
        .catch(next);
});

app.post('/purchase', trapsAPIValidator.purchase, function(req, res, next) {
    log.info("traps purchase analytics called: ", req.body);

    req.body.id = req.user;

    PurchaseQueue.push(req.body)
        .then(function() {
            res.data = null;
            next();
        })
        .catch(next);
});