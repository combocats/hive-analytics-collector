var settings = require('hive-common').settings;
var log      = require('hive-common').logger('app');
var _        = require('lodash');

var express  = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var app = express();

app.use(bodyParser());
app.use(cookieParser());

/* Config session storage */
var RedisStore = require('connect-redis')(session);
var redis = require('redis');
var sessionClient = redis.createClient(settings.config.redis_port, settings.config.redis_host);

var sessionConf = {
    secret: 'hive secret cat',
    store: new RedisStore({client: sessionClient})
};

//CORS middleware for
app.use(require('hive-common').cors);

// token based authorization for CORS and non-cookie agents
app.use(require('hive-common').tokenAccess);

app.use(session(sessionConf));

/**
 * Mount all API modules using app.use method.
 * Here we can add any API prefix we need.
 */
app.use('/api/analytics', require('./lib/api_users_analytics'));
app.use('/api/analytics/traps', require('./lib/api_traps_analytics'));

/**
 * End point for all error requests to public API
 */
app.use('/api', require('hive-common').endPoints.error);

/**
 * End point for all non error requests to api
 */
app.use('/api', require('hive-common').endPoints.success);

/**
 * Check api for availability tests.
 */
app.get('/check', function(req, res) {
    res.json({ok: true});
});

var server = require('http').createServer(app);
server.listen(settings.config.port, settings.config.host);
log.info("listen on %s", settings.config.port);

